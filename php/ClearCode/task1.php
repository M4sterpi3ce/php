<?php

/**
 * @author Marcin Jachyra
 */
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $file = $_FILES["file"]["name"];
    $field = $_POST["field"];
    $success = $_POST["success"];
    
    require_once("class/task1.php");
    
    $task = new task1;
    if($task->validate($file, $field, $success))
        $task->group_by($file, $field, $success);
    else echo "Wrong inputs";
} else {
?>

<html>
    <head>
        <title>TASK 1</title>
    </head>
    <body>
        <form action="#" method="POST" enctype="multipart/form-data">
            <input type="file" name="file"/><br>
            *<input type="radio" name="field" value="year" id="year" required/> <label for="year">Year</label> <input type="radio" name="field" value="month" id="month" required/> <label for="month">Month</label><br>
            *<input type="radio" name="success" id="none" value="None" required/> <label for="none">Don't filter</label> <input type="radio" name="success" value="S" id="true" required/> <label for="true">Success</label> <input type="radio" name="success" value="F" id="false" required/> <label for="false">Failed</label><br>
            <input type="submit"/>
                
        </form>
        * - required
    </body>
</html>

<?php
}