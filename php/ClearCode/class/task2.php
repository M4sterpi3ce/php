<?php
/**
 * @author Marcin Jachyra
 */
class task2 {
    private $spells = array();
    private $subspells = array(
            "dai" => 5,
            "jee" => 3,
            "ain" => 3,
            "je" => 2,
            "ne" => 2,
            "ai" => 2
        );
    
    private function getDamage($spell, $sort)
    {
        if($sort == 1) asort($this->subspells);
        $damage = 3; // skoro czar jest prawidłowy to jego moc zaczyna się od 3
        foreach($this->subspells as $key => $val){ // dla każdego podczaru
            $spell = str_replace($key, "", $spell, $count); // jeśli znajduje się w stringu, to go stamtąd wycinamy i inkrementujemy zmienną $count
            $damage += $val * $count; // zwiększamy wartość obrażeń o moc sprawdzanego podczaru * ilość wystąpień w stringu
        }
        $damage -= strlen($spell); // odejmujemy od obrażeń ilość pozostałych znaków w stringu
        if($damage < 0) $damage = 0; // jeśli ilość obrażeń jest minusowa, to zmieniamy wartość na 0
        $this->spells[] = $damage; // wynik do tablicy
    }
    
    public function calculateDamage($spell)
    {
        if(preg_match("/^(?:(?!fe)[a-z])*fe{1}(?:(?!fe)[a-z])*ai{1}(?:(?!fe)[a-z])*$/", $spell)){ //sprawdzenie czy wprowadzony łańcuch znakowy to poprawny czar
            $spells = array();
            $spell = substr($spell, strpos($spell, "fe")+2, strrpos($spell, "ai") - strpos($spell, "fe")-2); // znalezienie takiego podstringa który zaczyna się od fe i kończy na ostatnie ai, nie wliczając w to tych podczarów.
             
            $this->getDamage($spell, 0); // obliczenie obrażeń zaczynając od najmocniejszych podczarów
            $this->getDamage($spell, 1); // obliczenie obrażeń zaczynając od najsłabszych podczarów
            rsort($this->spells); // posortowanie tablicy malejąco
            return $this->spells[0]; // zwrócenie pierwszego wyniku tablicy (największego)
        } else return 0; // jeśli czar jest niepoprawny to zwróć wartość obrażeń równą 0
    }
}
