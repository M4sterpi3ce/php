<?php
/**
 * @author Marcin Jachyra
 */
class task1 {
    public function group_by($stream, $field, $success="None")
    {
        $groups = array();
        if($stream != null) $lines = file($stream); // nie trzeba podawać pliku. 
        else $lines = file("launchlog.txt"); // Jeśli nie zostanie podany, to weźmie z katalogu
        $i = 2; // zaczynamy od trzeciego wiersza
        while($i < count($lines)){ // sprawdzamy każdy wiersz pliku
            $parts = preg_split("/[\s]{3,}/", $lines[$i]); // dzielimy każdy wiersz na części. Separatorem są minimum 3 spacje
            if(count($parts) < 7){ // jeśli części jest mniej niż 7 to znaczy, że nie ma informacji o dacie startu, ani o powodzeniu misji
                $a = $i; // zmienna pomocnicza
                do{
                    $parts = preg_split("/[\s]{3,}/", $lines[--$a]);
                }while(count($parts) < 7); // dzielimy na części poprzednie wiersze, aż natrafimy na taki wiersz który ma przynajmniej 7 części
            }
            
            $suc = $parts[count($parts)-2]; // przedostatnia kolumna wiersza, czyli wyznacznik sukcesu misji
            
            if($suc == $success || $success == "None"){ // filtrowanie wyniku pod względem sukcesu, porażki lub braku filtrowania
                $date = explode(" ", $parts[1]); // podzielenie drugiej kolumny wiersza na dwie istotne części. Spacja jest separatorem
                if($field == "year"){ // w przypadku filtrowania po roku startu...
                    $date = $date[0]; // pierwsza część drugiej kolumny
                } else { // w przeciwnym wypadku, czyli filtrowanie po miesiącu...
                    $date = $date[1]; // ...druga część drugiej kolumny
                }
                
                if(array_key_exists($date, $groups)){ // jeśli istnieje klucz o takim roku lub miesiącu
                    $groups[$date]++; // to zwiększenie wartości o jeden
                } else { // w przeciwnym wypadku jeśli nie istnieje
                    $groups[$date] = 1; // to utworzenie takiego klucza z przypisaną wartością równą 1
                }
            }
            $i++; // następny wiersz
        }
        ksort($groups); // sortowanie kluczy tablicy od najmniejszego do największego
        $i = 0; // zmienna pomocnicza, dla ładniejszego wyświetlania przecinków
        foreach($groups as $key => $value){ // dla każdego klucza tablicy $groups
            echo "'$key': $value"; // wyświetlanie klucza i wartości
            if(++$i != count($groups)) echo ",<br>"; // oddzielanie przecinkiem dopóki nie natrafimy na ostatni element tablicy
            else echo "."; // gdy natrafimy, to stawiamy kropkę
        }
    }
    
    public function validate($stream, $field, $success)
    {
        if(isset($stream) && isset($field) && isset($success)){
            // walidacja pliku
            if($stream != null){
                $extension = explode(".", $stream);
                $extension = $extension[count($extension)-1];
                if($extension != "txt") return false;
            }
            //walidacja fieldu
            if($field != "year" && $field != "month") return false;
            //walidacja sukcesu
            if($success != "None" && $success != "S" && $success != "F") return false;
            
            return true;
        } else return false;
    }
}