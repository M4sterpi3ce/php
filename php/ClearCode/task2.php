<?php

/**
 * @author Marcin Jachyra
 */
if($_SERVER["REQUEST_METHOD"] == "POST"){
    $spell = $_POST["spell"];
    
    require_once("class/task2.php");
    
    $task = new task2;
    echo "damage('$spell') == " . $task->calculateDamage($spell);
} else {
?>

<html>
    <head>
        <title>TASK 2</title>
    </head>
    <body>
        <form action="#" method="POST">
        Spell: <input type="text" name="spell" />
        <input type="submit" value="Submit" />
        </form>
    </body>
</html>

<?php
}